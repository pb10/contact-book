import db


class Menu:
    '''A class used to represent a generic menu interface
    ...
    Attributes
    ----------
    options: list
        A list of options that are available for the menu
    '''
    def __init__(self):
        self.options = ['1. Add a contact', '2. Edit a contact', '3. Delete a contact',
                        '4. Search for contact', '5. Browse']


class CMDMenu(Menu):
    '''A Command Line interface for the contact book
    ...
    Methods
    -------
    print_menu()
        Displays the main menu

    add_a_contact()
        Adds a contacts details to the contact book

    edit_contact()
        Allows the user to update a contacts details

    edit_submenu()
        Displays the options available to be edited

    delete_contact
        Deletes a contacts details from the contact book

    search
        Allow a user to search for a contact via the specified option

    browse
        View all contacts in the book or display results by surname

    result_printer
        Formats the results returned from the database
    '''
    def print_menu(self):
        print('Contact Book')
        for option in self.options:
            print(option)

    def add_a_contact(self):
        print("Add Contact details: ")
        print("Please note, first name and surname are required. Other details are optional\n")
        first = input("Enter First Name (Required): ")
        surname = input("Enter Surname (Required): ")

        if first == '' or surname == '':
            print('First name and Surname are required')
            return

        mobile = input("Enter mobile number: ")
        phone = input("Enter home phone number: ")
        email = input("Enter email address: ")
        address_one = input("Enter first line of address: ")
        address_two = input("Enter second line of address: ")
        town = input("Enter Town: ")
        city = input("Enter City: ")
        postcode = input("Enter Postcode: ")

        contact = {'first_name': first, 'surname': surname, 'mobile': mobile, 'phone': phone,
                   'email': email, 'addr_one': address_one, 'addr_two': address_two,
                   'town': town, 'city': city, 'postcode': postcode}

        for k, v in contact.items():
            if v == '':
                contact[k] = None

        db.add_record(contact)

    def edit_contact(self):
        id = input("Enter ID of contact that needs editing: ")
        statement = f'SELECT * FROM contact WHERE id=?'
        db.cur.execute(statement, (id,))
        result = db.cur.fetchone()

        if not result:
            print('User ID not found. Please use the search to confirm the number')
            return

        print(f'ID | First | Surname | Home Phone | Mobile | Email | Addr 1 | Addr 2 | Town | City | Postcode')
        print(result)

        self.edit_submenu()

        options = {'1': 'first_name', '2': 'surname', '3': 'mobile', '4': 'phone', '5': 'email',
                   '6': 'addr_one', '7': 'addr_two', '8': 'town', '9': 'city', '10': 'postcode'}

        choice = input("Which information requires editing? [1-10] ")
        if choice in options.keys():
            updated_info = input(f'Please enter the updated information for {options[choice]} ')
            if updated_info == '' and (choice == '1' or choice == '2'):
                print("Name is required")
                return 
            db.update_record(options, choice, updated_info, id)
        else:
            print('Option not found. Must be between 1-10')
            return


    def edit_submenu(self):
        options = ['1. First name', '2. Surname', '3. Mobile number', '4. Home telephone',  '5. Email',
                   '6. Address Line 1', '7. Address Line 2', '8. Town', '9. City', '10. Postcode']
        print('Edit details (0 to exit)')
        for option in options:
            print(option)



    def delete_contact(self):
        name = input("Enter the surname of the contact you would like to delete ")
        statement = 'SELECT * FROM contact WHERE surname=?'

        for row in db.cur.execute(statement, (name.title(),)):
            print(row)

        id = input("Enter contact ID to delete (or 0 to exit) ")
        if id == '0':
            return
        else:
            print("Please confirm that you want to delete: ")
            db.cur.execute('SELECT first_name, surname FROM contact WHERE id=?', id)

            choice = input('Yes or No? ').lower()
            if choice == 'yes' or choice == 'y':
                db.delete_record(id)
            else:
                print("No record deleted")
                return

    def search(self):
        options = ['1. First Name', '2. Surname', '3. Mobile Number', '4. City']
        for option in options:
            print(option)
        print()
        choice = input("Search by: (Enter a number)\n")
        if choice == '1':
            choice = 'first_name'
        elif choice == '2':
            choice = 'surname'
        elif choice == '3':
            choice = 'mobile'
        elif choice == '4':
            choice = 'city'
        else:
            print("Number must be between 1-4.")
            return

        term = input(f"Please enter the search term.\nYou are searching by {choice} ")
        statement = f'SELECT * FROM contact WHERE {choice}=?'
        db.cur.execute(statement, (term.title(),))
        results = db.cur.fetchall()

        if not results:
            print("No results found")
            return

        self.result_printer(results)

    def browse(self):
        print("1. View all contacts")
        print("2. View by Surname Letter")
        choice = input("View all contacts [1] or by surname [2]? ")

        if choice == '1':
            statement = 'SELECT * FROM contact ORDER BY surname'
            db.cur.execute(statement)
            results = db.cur.fetchall()

            self.result_printer(results)

        elif choice == '2':
            print('A B C D E F G H I J K L M N O P Q R S T U V W X Y Z')
            letter = input('Select first letter of surname: ')
            statement = 'SELECT * FROM contact WHERE surname LIKE ?'
            db.cur.execute(statement, (f'{letter}%',))
            results = db.cur.fetchall()

            if not results:
                print('No results found')
                return

            self.result_printer(results)

        else:
            print('Not a valid choice')
            return

    def result_printer(self, results):
        for record in results:
            for item in record:
                if item:
                    print(item)
            print('-----------')








