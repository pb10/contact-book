import menu

cmd_menu = menu.CMDMenu()

options = {'1': cmd_menu.add_a_contact, '2': cmd_menu.edit_contact, '3': cmd_menu.delete_contact,
           '4': cmd_menu.search, '5': cmd_menu.browse}

while True:
    cmd_menu.print_menu()
    print()
    choice = input("Choose an option [1-5] or 0 to exit ")
    if choice == '0':
        break
    elif choice in options.keys():
        options[choice]()
    else:
        print("Not a valid option")
    print()

