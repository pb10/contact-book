import sqlite3

connect = sqlite3.connect('contactBook.db')
cur = connect.cursor()

cur.execute('CREATE TABLE IF NOT EXISTS contact '
            '(id INTEGER PRIMARY KEY AUTOINCREMENT, first_name TEXT NOT NULL, surname TEXT NOT NULL,'
            'mobile TEXT, phone TEXT, email TEXT, '
            'addr_one TEXT, addr_two TEXT, town TEXT, city TEXT, postcode TEXT)')


def add_record(contact_details):
    provided_details = {}

    for k, v in contact_details.items():
        if v:
            provided_details[k] = v

    keys = ','.join(provided_details.keys())
    values = tuple(provided_details.values())
    placeholder = ','.join(list('?'*len(provided_details)))

    statement = f'INSERT INTO contact ({keys}) VALUES ({placeholder})'

    cur.execute(statement, values)
    connect.commit()


def update_record(options, choice, updated_info, id):
    statement = f'UPDATE contact SET {options[choice]}=? WHERE id=?'
    cur.execute(statement, (updated_info, id))
    connect.commit()


def delete_record(id):
    cur.execute('DELETE FROM contact WHERE id=?', id)
    connect.commit()
